# Keycloak MediaWiki B-type password patch

## Synopsis
This repository contains a patch file which provides a PasswordHashProvider and
its factory for the old MediaWiki B-type password.

It was tested with the following Keycloack versions:

- 3.1.x
- 3.2.x
- 3.3.x
- 3.4.x

## License
This project is licensed under the LGPLv3. See the [LICENSE.md](LICENSE.md) file for details.
